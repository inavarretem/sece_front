import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { SignupComponent } from './signup/signup.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { GestorAccesoComponent } from './gestor-acceso/gestor-acceso.component';
import { GestorAdministrativoComponent } from './gestor-administrativo/gestor-administrativo.component';
import { GestorClinicoComponent } from './gestor-clinico/gestor-clinico.component';

import { ConsultaComponent } from './gestor-clinico/consulta/consulta.component';
import { FarmaciaComponent } from './gestor-clinico/farmacia/farmacia.component';
import { HospitalizacionComponent } from './gestor-clinico/hospitalizacion/hospitalizacion.component';
import { ImagenologiaComponent } from './gestor-clinico/imagenologia/imagenologia.component';
import { LaboratorioComponent } from './gestor-clinico/laboratorio/laboratorio.component';
import { QuirofanoComponent } from './gestor-clinico/quirofano/quirofano.component';
import { UrgenciasComponent } from './gestor-clinico/urgencias/urgencias.component';

import { AgendasComponent } from './gestor-administrativo/agendas/agendas.component';
import { CajasComponent } from './gestor-administrativo/cajas/cajas.component';
import { FacturacionComponent } from './gestor-administrativo/facturacion/facturacion.component';
import { InventarioComponent } from './gestor-administrativo/inventario/inventario.component';
import { RegistroPacienteComponent } from './gestor-administrativo/registro-paciente/registro-paciente.component';

import { CatalogComponent } from './gestor-acceso/catalog/catalog.component';
import { ConfigComponent } from './gestor-acceso/config/config.component';
import { DiagComponent } from './gestor-acceso/diag/diag.component';
import { RolesComponent } from './gestor-acceso/roles/roles.component';
import { UsersComponent } from './gestor-acceso/users/users.component';

import { AuthGuardGuard } from './_guard/auth-guard.guard'
import { ClinicGuard } from './_guard/clinic.guard'
import { AdminGuard } from './_guard/admin.guard'

const routes: Routes =[
    { path: 'login',           component: LoginComponent },
    { path: 'inicio',          component: LandingComponent },
    { path: 'gestor-acceso',   component: GestorAccesoComponent, canActivate: [AuthGuardGuard] },

    { path: 'acceso/roles',   component: RolesComponent, canActivate: [AuthGuardGuard] },
    { path: 'acceso/users',   component: UsersComponent, canActivate: [AuthGuardGuard] },
    { path: 'acceso/diagnosis',   component: DiagComponent, canActivate: [AuthGuardGuard] },
    { path: 'acceso/config',   component: ConfigComponent, canActivate: [AuthGuardGuard] },
    { path: 'acceso/catalog',   component: CatalogComponent, canActivate: [AuthGuardGuard] },

    { path: 'gestor-clinico',   component: GestorClinicoComponent, canActivate: [ClinicGuard] },
    { path: 'clinica/consulta',   component: ConsultaComponent, canActivate: [ClinicGuard] },
    { path: 'clinica/hospitalizacion',   component: HospitalizacionComponent, canActivate: [ClinicGuard] },
    { path: 'clinica/urgencias',   component: UrgenciasComponent, canActivate: [ClinicGuard] },
    { path: 'clinica/farmacia',   component: FarmaciaComponent, canActivate: [ClinicGuard] },
    { path: 'clinica/laboratorio',   component: LaboratorioComponent, canActivate: [ClinicGuard] },
    { path: 'clinica/imagenologia',   component: ImagenologiaComponent, canActivate: [ClinicGuard] },
    { path: 'clinica/quirofano',   component: QuirofanoComponent, canActivate: [ClinicGuard] },

    { path: 'gestor-administrativo',   component: GestorAdministrativoComponent, canActivate: [AdminGuard] },

    { path: 'administrativo/pacientes',   component: RegistroPacienteComponent, canActivate: [AdminGuard] },
    { path: 'administrativo/agendas',   component: AgendasComponent, canActivate: [AdminGuard] },
    { path: 'administrativo/cajas',   component: CajasComponent, canActivate: [AdminGuard] },
    { path: 'administrativo/inventarios',   component: InventarioComponent, canActivate: [AdminGuard] },
    { path: 'administrativo/facturacion',   component: FacturacionComponent, canActivate: [AdminGuard] },

    // { path: 'home',             component: HomeComponent },
    // { path: 'user-profile',     component: ProfileComponent },
    // { path: 'register',           component: SignupComponent },
    // { path: 'inicio',          component: LandingComponent },

    { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
      useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
