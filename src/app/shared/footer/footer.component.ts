import { Component, OnInit } from '@angular/core';
import { SeceServiceService } from '../../services/sece-service.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
    test : Date = new Date();
    access : String;
    clinicCount : number = 0;
    accessCount : number = 0;
    adminCount : number = 0;
    clinicAccess: boolean = false;
    adminAccess: boolean = false;
    accesAccess: boolean = false;
    constructor(private _service: SeceServiceService, private router: Router ) {}

    ngOnInit() {
      this.clinicCount = 0;
      this.accessCount = 0;
      this.adminCount = 0;
      // this.checkClinicPermission();
      // this.checkAdminPermission();
      // this.checkAccessPermission();
    }
    getPath(){
      return this.router.url;
    }
    // checkClinicPermission(){
    //
    //   if (this.clinicCount > 1){
    //     return this.clinicAccess;
    //   }
    //   else {
    //     this._service.clinic()
    //      .subscribe(
    //        data => {
    //          if (data.access){
    //            console.log(data.access)
    //            this.clinicAccess = true;
    //            this.clinicCount++;
    //            console.log(this.clinicCount)
    //            return this.clinicAccess;
    //          }
    //          else{
    //            this.clinicCount++;
    //            console.log(data.access)
    //            this.clinicAccess = false;
    //            return this.clinicAccess;
    //          }
    //        },
    //        error => {
    //           this.clinicAccess = false
    //           this.clinicCount++;
    //           return  this.clinicAccess;
    //
    //        }
    //
    //      )
    //
    //   }
    // }
    // checkAdminPermission(){
    //   if (this.adminCount > 1){
    //     return this.adminAccess;
    //   }
    //   else {
    //     this._service.admin()
    //      .subscribe(
    //        data => {
    //          if (data.access){
    //            console.log(data.access)
    //            this.adminAccess = true;
    //            this.adminCount++;
    //            console.log(this.adminCount)
    //            return this.adminAccess;
    //          }
    //          else{
    //            this.adminCount++;
    //            console.log(data.access)
    //            this.adminAccess = false;
    //            return this.adminAccess;
    //          }
    //        },
    //        error => {
    //           this.adminAccess = false
    //           this.adminCount++;
    //           return  this.adminAccess;
    //
    //        }
    //      )
    //
    //   }
    // }
    // checkAccessPermission(){
    //   if (this.accessCount > 1){
    //     return this.accesAccess;
    //   }
    //   else {
    //     this._service.access()
    //      .subscribe(
    //        data => {
    //          if (data.access){
    //            console.log(data.access)
    //            this.accesAccess = true;
    //            this.accessCount++;
    //            console.log(this.accessCount)
    //            return this.accesAccess;
    //          }
    //          else{
    //            this.accessCount++;
    //            console.log(data.access)
    //            this.accesAccess = false;
    //            return this.accesAccess;
    //          }
    //        },
    //        error => {
    //           this.accesAccess = false
    //           this.accessCount++;
    //           return  this.accesAccess;
    //
    //        }
    //      )
    //
    //   }
    // }
}
