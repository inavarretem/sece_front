import { Injectable } from '@angular/core';
import { Router,CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { SeceServiceService } from '../services/sece-service.service';
import { catchError, map} from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

@Injectable({
  providedIn: 'root'
})
export class ClinicGuard implements CanActivate {
  currentUser:any = {
    username: '',
    id:0,
    accessClinic: "",
    accessAccess: "",
    accessAdmin: ""
  };
  constructor(private _service:SeceServiceService,private _router:Router){ }
  canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot):any {
    return this._service.getUsername().pipe(
      map( e => {
        if(e.accessClinic === "Y"){
          return true
        } else{
          return false;
        }
      }),catchError((err) => {
        this._router.navigate(['inicio']);
        return of (false)
      })
    )
  }

}
