import { TestBed, async, inject } from '@angular/core/testing';

import { ClinicGuard } from './clinic.guard';

describe('ClinicGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClinicGuard]
    });
  });

  it('should ...', inject([ClinicGuard], (guard: ClinicGuard) => {
    expect(guard).toBeTruthy();
  }));
});
