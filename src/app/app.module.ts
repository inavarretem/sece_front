import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';

import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HomeModule } from './home/home.module';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http'
//import SeceServiceService
import { SeceServiceService } from './services/sece-service.service';
import { GestorAccesoComponent } from './gestor-acceso/gestor-acceso.component';
import { GestorClinicoComponent } from './gestor-clinico/gestor-clinico.component';
import { GestorAdministrativoComponent } from './gestor-administrativo/gestor-administrativo.component';
import { AccessRolesComponent } from './access-roles/access-roles.component';
import { RolesComponent } from './gestor-acceso/roles/roles.component';
import { UsersComponent } from './gestor-acceso/users/users.component';
import { DiagComponent } from './gestor-acceso/diag/diag.component';
import { ConfigComponent } from './gestor-acceso/config/config.component';
import { CatalogComponent } from './gestor-acceso/catalog/catalog.component';
import { RegistroPacienteComponent } from './gestor-administrativo/registro-paciente/registro-paciente.component';
import { AgendasComponent } from './gestor-administrativo/agendas/agendas.component';
import { CajasComponent } from './gestor-administrativo/cajas/cajas.component';
import { InventarioComponent } from './gestor-administrativo/inventario/inventario.component';
import { FacturacionComponent } from './gestor-administrativo/facturacion/facturacion.component';
import { ConsultaComponent } from './gestor-clinico/consulta/consulta.component';
import { HospitalizacionComponent } from './gestor-clinico/hospitalizacion/hospitalizacion.component';
import { UrgenciasComponent } from './gestor-clinico/urgencias/urgencias.component';
import { FarmaciaComponent } from './gestor-clinico/farmacia/farmacia.component';
import { LaboratorioComponent } from './gestor-clinico/laboratorio/laboratorio.component';
import { ImagenologiaComponent } from './gestor-clinico/imagenologia/imagenologia.component';
import { QuirofanoComponent } from './gestor-clinico/quirofano/quirofano.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ScheduleModule, RecurrenceEditorModule, DayService,WeekService,WorkWeekService,MonthService,MonthAgendaService } from '@syncfusion/ej2-angular-schedule';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LandingComponent,
    ProfileComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    GestorAccesoComponent,
    GestorClinicoComponent,
    GestorAdministrativoComponent,
    AccessRolesComponent,
    RolesComponent,
    UsersComponent,
    DiagComponent,
    ConfigComponent,
    CatalogComponent,
    RegistroPacienteComponent,
    AgendasComponent,
    CajasComponent,
    InventarioComponent,
    FacturacionComponent,
    ConsultaComponent,
    HospitalizacionComponent,
    UrgenciasComponent,
    FarmaciaComponent,
    LaboratorioComponent,
    ImagenologiaComponent,
    QuirofanoComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    RouterModule,
    AppRoutingModule,
    HomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    Ng2SmartTableModule,
    ScheduleModule, RecurrenceEditorModule,
  ],
  providers: [SeceServiceService, DayService,WeekService,WorkWeekService,MonthService,MonthAgendaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
