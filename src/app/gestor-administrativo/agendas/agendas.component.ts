import { Component, ViewChild,OnInit } from '@angular/core';
import { SeceServiceService } from '../../services/sece-service.service';
import { Router,ActivatedRoute } from '@angular/router';
import { View,EventSettingsModel,ActionEventArgs,ScheduleComponent } from "@syncfusion/ej2-angular-schedule"
import { DataManager } from '@syncfusion/ej2-data';
const token = localStorage.getItem('token');
@Component({
  selector: 'app-agendas',
  templateUrl: './agendas.component.html',
  styleUrls: ['./agendas.component.css']
})
export class AgendasComponent implements OnInit {
  constructor(private _service:SeceServiceService,private _router:Router) {
    this._service.getUsername()
    .subscribe(
      (data: any) => {
        this.currentUser=data;
        this.currentUser.username = data.username;
      },
      error => this._router.navigate(['login'])
    )
  }
  public data: DataManager;
  currentUser:any = {
    username: '',
    id:0,
    accessClinic: "",
    accessAccess: "",
    accessAdmin: ""
  };

  @ViewChild('scheduleObj')
  public scheduleObj: ScheduleComponent;
  public setView: View = 'Month'
  private dataManager: DataManager = new DataManager({
       url: 'http://localhost:3000/getAppointments',
       crossDomain: false
    });
  public eventSettings: EventSettingsModel = { dataSource: this.dataManager};
  onActionBegin(args: ActionEventArgs): void {
    if (args.requestType === 'eventChange') { //while editing the existing event
      console.log("event edit");
    }
    if (args.requestType === 'eventCreate') { //while creating new event
      console.log("event create");
      console.log(args.data);
      this._service.addAppoinment(args.data)
      .subscribe(
        (data: any) => {
          window.alert("Saved!");
          // this.currentUser=data;
          // this.currentUser.username = data.username;
        },
        error => window.alert("Error Saving!")
      )
    }
    if (args.requestType === 'saveEvent') { //while creating new event
      console.log("event Save");
    }
  }
  checkSecRoleRights(){
    return true;
  }
  logout(){
    localStorage.removeItem('token');
    this._router.navigate(['login'])
  }
  getPath(){
    return this._router.url;
  }
  checkClinicPermission(){
    if(this.currentUser.accessClinic === "Y"){
      return true;
    }
  }
  checkAdminPermission(){
    if(this.currentUser.accessAdmin === "Y"){
      return true;
    }
  }
  checkAccessPermission(){
    if(this.currentUser.accessAccess === "Y"){
      return true;
    }
  }
  ngOnInit() {}

}
