import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestorAdministrativoComponent } from './gestor-administrativo.component';

describe('GestorAdministrativoComponent', () => {
  let component: GestorAdministrativoComponent;
  let fixture: ComponentFixture<GestorAdministrativoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestorAdministrativoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestorAdministrativoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
