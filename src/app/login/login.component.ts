import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { SeceServiceService } from '../services/sece-service.service';
import { Router,ActivatedRoute } from '@angular/router';

interface Obj {
  message: String;
  access: String;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  focus;
  focus1;
  loginForm:FormGroup;
  loginMessage = '';

  constructor(private _service:SeceServiceService,private _router:Router) {
    if (localStorage.getItem('token')) {
      this._service.getUsername()
      .subscribe(
        data => {
          this._router.navigate(['inicio'])
        },
        error => {
          console.log(error);

          this._router.navigate(['login'])
        }
      )
    } else {
      this._router.navigate(['login'])
    }
    this.loginForm = new FormGroup({
      site: new FormControl(null, Validators.required),
      id: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  ngOnInit() {
  }
  Login() {
    if(this.loginForm.valid){
      this._service.login(this.loginForm.value).subscribe(
        (data: any) => {
          if('message' in data)
          this.loginMessage = data.message
          else {
            if ('access' in data as any)
            localStorage.setItem('token',data.access.toString());
            this._router.navigate(['inicio']);
          }
        }
      )
    }
  }
}
