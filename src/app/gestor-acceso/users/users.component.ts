import { Component, OnInit } from '@angular/core';
import { Ng2SmartTableModule,LocalDataSource } from 'ng2-smart-table';
import { SeceServiceService } from '../../services/sece-service.service';
import { Router,ActivatedRoute } from '@angular/router';

export interface Section {
  name: string;
  updated: Date;
}
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})


export class UsersComponent implements OnInit {
  currentUser:any = {
    username: '',
    id:0,
    accessClinic: "",
    accessAccess: "",
    accessAdmin: ""
  };
  settings: Object;
  userG:any = [];
  constructor(private _service:SeceServiceService,private _router:Router) {
    this._service.getUsername()
    .subscribe(
      (data: any) => {
        this.currentUser=data;
        this.currentUser.username = data.username;
      },
      error => this._router.navigate(['login'])
    )
    this._service.getUserGroup()
    .subscribe(
      (data: any) => {

        data.forEach(element => {
          this.userG.push({value: element.UserG_strDesc,title:element.UserG_strDesc})
        });
        this.getList();
        this.settings = this.loadTableSettings();
      },
      error => this._router.navigate(['login'])
    )
   }
loadTableSettings(){
  return {
    // add: {
    //     addButtonContent: '<i class="ion-ios-plus-outline"></i>',
    //     createButtonContent: '<i class="ion-checkmark"></i>',
    //     cancelButtonContent: '<i class="ion-close"></i>',
    //     confirmCreate: true,
    //   },
    //   edit: {
    //     editButtonContent: '<i class="ion-edit"></i>',
    //     saveButtonContent: '<i class="ion-checkmark"></i>',
    //     cancelButtonContent: '<i class="ion-close"></i>',
    //     confirmSave: true,
    //   },
    mode: 'inline',
    add: {
      addButtonContent: '<i class="ni ni-fat-add"></i>',
      createButtonContent: '<i class="ni ni-send"></i>',
      cancelButtonContent: '<i class="ni ni-fat-remove"></i>',
      confirmCreate: true,
    },
    actions: { delete: false,
             add: false,
           },
    edit: {
        editButtonContent: '<i class="ni ni-ruler-pencil"></i>',
        saveButtonContent: '<i class="ni ni-like-2"></i>',
        cancelButtonContent: '<i class="ni ni-fat-remove"></i>',
        confirmSave: true
      },
    pager: {
      display: true,
      perPage: 50
    },
    columns: {
      User_intUserNo: {
        title: 'ID',
        type: 'number',
        editable: false,
      },
      User_strFirstName: {
        title: 'Nombre',
        type: 'string',
      },
      User_strLastName: {
        title: 'Apellido',
        type: 'string',
      },
      UserG_strDesc: {
        title: 'Rol',
        type: 'string',
        editor: {
          type: 'list',
          config: {
            list: this.userG
          }

        }
      },
      User_strPassword: {
        title: 'Password',
        type: 'password',
        editable: false,
        width:'15%'
      },

    }

  }
}


  ngOnInit() {

  }
  logout(){
    localStorage.removeItem('token');
    this._router.navigate(['login'])
  }
  getPath(){
    return this._router.url;
  }
  checkClinicPermission(){
    if(this.currentUser && this.currentUser.accessClinic === "Y"){
      return true;
    }
  }
  checkAdminPermission(){
    if(this.currentUser && this.currentUser.accessAdmin === "Y"){
      return true;
    }
  }
  checkAccessPermission(){
    if(this.currentUser && this.currentUser.accessAccess === "Y"){
      return true;
    }
  }
  source: LocalDataSource = new LocalDataSource();
  getList(): void{
    const tableData = this._service.getUsers().subscribe(data => {
      this.source.load(data);
    });
    // this.service.getItemsInfo()
    // .subscribe(data => {this.items} = data);
  }
  onEdit(event) {
    console.log(event);
  }
  onEditConfirm(event) {
    if (window.confirm('Seguro que deseas guardar los cambios?')) {
      //call to remote api, remember that you have to await this
      this._service.updateUser(event.newData).subscribe(
        (data: any) => {
          if (data[0] >= 1) {
            console.log("rowsAffected");
            event.confirm.resolve(event.newData);
          }
          else{
            window.alert('No se pudo actualizar el usuario');
          }

        }
      )
      //event.confirm.resolve(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirm(event){
    console.log(event)
    if (window.confirm('¿Agregar usuario?')) {
      //call to remote api, remember that you have to await this
      this._service.addUser(event.newData).subscribe(
        (data: any) => {
          if (data[0] >= 1) {
            console.log("rowsAffected");
            event.confirm.resolve(event.newData);
            this.getList();
          }
          else{
            window.alert('No se pudo agregar el usuario');
          }

        }
      )
    } else {
      event.confirm.reject();
    }
  }
}
