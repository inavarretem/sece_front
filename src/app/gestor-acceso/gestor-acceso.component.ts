import { Component, OnInit } from '@angular/core';
import { SeceServiceService } from '../services/sece-service.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-gestor-acceso',
  templateUrl: './gestor-acceso.component.html',
  styleUrls: ['./gestor-acceso.component.css']
})
export class GestorAccesoComponent implements OnInit {
  currentUser:any = {
    username: '',
    id:0,
    accessClinic: "",
    accessAccess: "",
    accessAdmin: ""
  };

  constructor(private _service:SeceServiceService,private _router:Router) {
    this._service.getUsername()
    .subscribe(
      (data: any) => {
        this.currentUser=data;
        this.currentUser.username = data.username;
      },
      error => this._router.navigate(['login'])
    )
  }
  logout(){
    localStorage.removeItem('token');
    this._router.navigate(['login'])
  }
  getPath(){
    return this._router.url;
  }
  checkClinicPermission(){
    if(this.currentUser && this.currentUser.accessClinic === "Y"){
      return true;
    }
  }
  checkAdminPermission(){
    if(this.currentUser && this.currentUser.accessAdmin === "Y"){
      return true;
    }
  }
  checkAccessPermission(){
    if(this.currentUser && this.currentUser.accessAccess === "Y"){
      return true;
    }
  }
  ngOnInit() {}

}
