import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestorAccesoComponent } from './gestor-acceso.component';

describe('GestorAccesoComponent', () => {
  let component: GestorAccesoComponent;
  let fixture: ComponentFixture<GestorAccesoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestorAccesoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestorAccesoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
