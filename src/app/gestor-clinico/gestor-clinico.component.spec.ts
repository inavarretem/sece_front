import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestorClinicoComponent } from './gestor-clinico.component';

describe('GestorClinicoComponent', () => {
  let component: GestorClinicoComponent;
  let fixture: ComponentFixture<GestorClinicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestorClinicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestorClinicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
