import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagenologiaComponent } from './imagenologia.component';

describe('ImagenologiaComponent', () => {
  let component: ImagenologiaComponent;
  let fixture: ComponentFixture<ImagenologiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagenologiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagenologiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
