import { Injectable } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http'
//
// interface login {
//   message: string;
//   access: string;
// }
// interface token {
//   username: string;
// }
@Injectable({
  providedIn: 'root'
})
export class SeceServiceService {

  constructor(private _http:HttpClient) { }

  login(body:any){
    return this._http.post<any>('http://localhost:3000/login',body,{
      observe:'body'
    })
  }
  addUser(body:any){
    return this._http.post<any>('http://localhost:3000/addUser',body,{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }
  addAppoinment(body:any){
    return this._http.post<any>('http://localhost:3000/addAppoinment',body,{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }
  insertPatient(body:any){
    return this._http.post<any>('http://localhost:3000/addPatient',body,{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }
  updateUser(body:any){
    return this._http.post<any>('http://localhost:3000/updateUser',body,{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }

  getUsername(){
    return this._http.get<any>('http://localhost:3000/username',{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }
  getAppointments(){
    return this._http.get<any>('http://localhost:3000/getAppointments',{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }
  clinic(){
    return this._http.get<any>('http://localhost:3000/checkClinicPermission',{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }
  admin(){
    return this._http.get<any>('http://localhost:3000/checkAdminPermission',{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }
  access(){
    return this._http.get<any>('http://localhost:3000/checkAccessPermission',{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }
  getUsers(){
    return this._http.get<any>('http://localhost:3000/getUsers',{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }
  getUserGroup(){
    return this._http.get<any>('http://localhost:3000/getUserGroup',{
      observe:'body',
      params: new HttpParams().append('token',localStorage.getItem('token'))
    })
  }
}
