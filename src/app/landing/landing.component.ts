import { Component, OnInit } from '@angular/core';
import { SeceServiceService } from '../services/sece-service.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})

export class LandingComponent implements OnInit {
  currentUser:any = {
    username: '',
    id:0,
    accessClinic: "",
    accessAccess: "",
    accessAdmin: "",
    SiteName: ""
  };

  constructor(private _service:SeceServiceService,private _router:Router) {
    this._service.getUsername()
    .subscribe(
      (data: any) => {
        this.currentUser=data;
        this.currentUser.username = data.username;
        this.currentUser.SiteName = data.sitename;
      },
      error => this._router.navigate(['login'])
    )
  }
  checkSecRoleRights(){
    return true;
  }
  logout(){
    localStorage.removeItem('token');
    this._router.navigate(['login'])
  }
  getPath(){
    return this._router.url;
  }
  checkClinicPermission(){
    if(this.currentUser.accessClinic === "Y"){
      return true;
    }
  }
  checkAdminPermission(){
    if(this.currentUser.accessAdmin === "Y"){
      return true;
    }
  }
  checkAccessPermission(){
    if(this.currentUser.accessAccess === "Y"){
      return true;
    }
  }
  ngOnInit() {}
}
