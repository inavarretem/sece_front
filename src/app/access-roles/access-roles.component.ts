import { Component, OnInit } from '@angular/core';
import { SeceServiceService } from '../services/sece-service.service';
import { Router,ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-access-roles',
  templateUrl: './access-roles.component.html',
  styleUrls: ['./access-roles.component.css']
})
export class AccessRolesComponent implements OnInit {

  username='';
  constructor(private _service:SeceServiceService,private _router:Router) {
    this._service.getUsername()
    .subscribe(
      data => {
        this.username = data.username;
      },
      error => this._router.navigate(['login'])
    )
  }
  // checkClinicPermission(){
  //   return true;
  // }
  // checkAdminPermission(){
  //   return true;
  // }
  // checkAccessPermission(){
  //   return true;
  // }
  logout(){
    localStorage.removeItem('token');
    this._router.navigate(['login'])
  }
  ngOnInit() {}

}
